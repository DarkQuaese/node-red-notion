module.exports = function(RED) {
    function NotionConfigNode(n) {
        RED.nodes.createNode(this,n);
        this.apiKey = n.apiKey;
    }
    RED.nodes.registerType("notion integration",NotionConfigNode);
}